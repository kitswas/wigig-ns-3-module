#! /usr/bin/env python3

# A list of C++ examples to run in order to ensure that they remain
# buildable and runnable over time.  Each tuple in the list contains
#
#     (example_name, do_run, do_valgrind_run).
#
# See test.py for more information.
cpp_examples = [
    ("wigig-compare-access-schemes-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt", "True", "False"),
    ("wigig-evaluate-achievable-throughput-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt --simulationTime=0.1", "True", "False"),
    ("wigig-evaluate-beam-refinement-protocol-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt", "True", "False"),
    ("wigig-evaluate-beamforming-cbap-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt", "True", "False"),
    ("wigig-evaluate-beamforming-itxss-rtxss-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt", "True", "False"),
    ("wigig-evaluate-codebook-analytical-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt", "True", "False"),
    ("wigig-evaluate-multi-service-period-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt", "True", "False"),
    ("wigig-evaluate-numerical-codebook-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt --codebookFolder=../../contrib/wigig/model/reference/Codebook", "True", "False"),
    ("wigig-evaluate-per-vs-snr-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt --simulationTime=0.1", "True", "False"),
    ("wigig-evaluate-qd-channel-lroom-scenario-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt --qdModelFolder=../../contrib/wigig/model/reference/QdChannel/L-ShapedRoom --codebookFolder=../../contrib/wigig/model/reference/Codebook --simulationTime=0.1", "True", "False"),
    ("wigig-evaluate-qd-channel-spatial-sharing --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt --qdBaseChannelFolder=../../contrib/wigig/model/reference/QdChannel --codebookFolder=../../contrib/wigig/model/reference/Codebook", "True", "False"),
    ("wigig-evaluate-qd-dense-scenario-single-ap-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt --qdBaseChannelFolder=../../contrib/wigig/model/reference/QdChannel --codebookFolder=../../contrib/wigig/model/reference/Codebook", "True", "False"),
    ("wigig-evaluate-talon-throughput-example-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt --codebookFolder=../../contrib/wigig/model/reference/Codebook", "True", "False"),
    ("wigig-evaluate-trafficstream-allocation-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt", "True", "False"),
    ("wigig-test-beamformdlink-maintenance-example --errorModel=../../contrib/wigig/model/reference/ErrorModel/LookupTable_1458.txt", "True", "False")
]
