# Module for simulating 802.11ad/ay (WiGig) devices in ns-3.

This is the completion of work previously
[started by Sebastien Deronne](https://www.nsnam.org/wp-content/uploads/2021/wigig-report-sept-2021.pdf)
and recently finished by Brian Swenson.
The [original wigig module](https://github.com/wigig-tools/wigig-module) was
created by Hany Assasa and IMDEA, but it contained a lot of code duplicated
from wifi module (which Sebastien has removed).

# Upstream repository

This module is based on commit 6287b50 from the [upstream repository](https://github.com/wigig-tools/wigig-module).

# Installation

Install this module into the `contrib` directory of ns-3 (3.40 release or later).

# Publications

Authors request to please cite previous [publications](https://github.com/wigig-tools/wigig-module#publications) if you use this module in your research.

# Contributing

Please submit merge requests and issues.  This module is only very lightly
maintained; contact the ns-3 project if you are interested in helping to
maintain it.
